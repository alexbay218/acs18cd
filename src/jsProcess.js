const fs = require('fs');

try { fs.mkdirSync('processed'); } catch(err){}

var perFile = (file) => {
  var data = JSON.parse(fs.readFileSync('data/' + file));
  var newData = [];
  if(data.length > 0) {
    var validKeys = Object.keys(data[0]);
    for(var i = 0;i < data.length;i++) {
      for(var j = 0;j < validKeys.length;j++) {
        if(typeof data[i][validKeys[j]] === 'undefined') {
          validKeys.splice(j,1);
          j--;
        }
      }
    }
    for(var i = 0;i < data.length;i++) {
      var newObj = {};
      for(var j = 0;j < validKeys.length;j++) {
        newObj[validKeys[j]] = data[i][validKeys[j]];
      }
      newData.push(newObj);
    }
    fs.writeFileSync('processed/' + file, JSON.stringify(newData, null, 2));
  }
};

fs.readdir('data', (err, files) => {
  if(err) { throw err; }
  for(var i = 0;i < files.length;i++) {
    perFile(files[i]);
  } 
});