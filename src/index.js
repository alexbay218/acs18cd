const census = require('citysdk');
const variables = require('./variables.json');
const fs = require('fs');

var exclude = [
  'for',
  'in',
  'ucgid',
  'UA',
  'US',
  'METDIV',
  'SUMLEVEL',
  'STATE',
  'LSAD_NAME',
  'NECTADIV',
  'GEOVARIANT',
  'SDELM',
  'GEOCOMP',
  'GEO_ID',
  'SDSEC',
  'SDUNI',
  'AIANHH',
  'ANRC',
  'PLACE',
  'PUMA5',
  'DIVISION',
  'NECTA',
  'REGION'
];
var features = Object.keys(variables.variables).filter((e) => {
  if(variables.variables[e].predicateType === 'int') {
    return !exclude.includes(e);
  }
  return false;
});
var statsKey = '';

try {
  statsKey = fs.readFileSync('key');
}
catch(err) {}

var main = async () => {
  var tmp = [];
  for(var i = 0;i < features.length;i++) {
    tmp.push(features[i]);
    if((i % 30 === 0 || i === features.length - 1) && i > 0) {
      tmp.push('NAME');
      try {
        var data = await (new Promise((resolve) => {
          census(
            {
              vintage: '2018',
              geoHierarchy: {
                state: '*',
                'congressional district': '*'
              },
              sourcePath: ['acs', 'acs1'],
              values: tmp,
              statsKey: statsKey
            },
            (err, res) => {
              if(err) {
                console.error(err);
              }
              resolve(res);
            }
          );
        }));
        for(var j = 0;j < data.length;j++) {
          var dataKeys = Object.keys(data[j]);
          for(var k = 0;k < dataKeys.length;k++) {
            if(data[j][dataKeys[k]] === 'NAN: null') {
              delete data[j][dataKeys[k]];
            }
            if(data[j][dataKeys[k]] === -999999999) {
              delete data[j][dataKeys[k]];
            }
            if(data[j][dataKeys[k]] === -888888888) {
              delete data[j][dataKeys[k]];
            }
            if(data[j][dataKeys[k]] === -666666666) {
              delete data[j][dataKeys[k]];
            }
            if(data[j][dataKeys[k]] === -555555555) {
              delete data[j][dataKeys[k]];
            }
            if(data[j][dataKeys[k]] === -333333333) {
              delete data[j][dataKeys[k]];
            }
            if(data[j][dataKeys[k]] === -222222222) {
              delete data[j][dataKeys[k]];
            }
          }
        }
        fs.writeFileSync('data/'+i+'.json', JSON.stringify(data, null, 2));
        console.log('' + (i/30) + ' out of ' + (features.length/30));
      }
      catch(err) {}
      tmp = [];
    }
  }
}

try { fs.mkdirSync('data'); } catch(err){}
main();