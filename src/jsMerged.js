const fs = require('fs');

fs.readdir('data', (err, files) => {
  if(err) { throw err; }
  var newData = [];
  if(files.length > 0) {
    newData = JSON.parse(fs.readFileSync('processed/' + files[0]));
    for(var i = 0;i < files.length;i++) {
      var tmpData = JSON.parse(fs.readFileSync('processed/' + files[i]));
      for(var j = 0;j < newData.length;j++) {
        for(var k = 0;k < tmpData.length;k++) {
          if(newData[j].state === tmpData[k].state && newData[j]['congressional-district'] === tmpData[k]['congressional-district']) {
            Object.assign(newData[j], tmpData[k]);
          }
        }
      }
    }
    fs.writeFileSync('merged.json', JSON.stringify(newData.sort((e1,e2) => {
      if(e1.state === e2.state) {
        return Number(e1['congressional-district']) - Number(e2['congressional-district']);
      }
      return Number(e1.state) - Number(e2.state);
    }), null, 2));
  }
});