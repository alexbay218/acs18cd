const fs = require('fs');
const variables = require('./variables.json');
var data = require('../merged.json');

var csv = '';

if(data.length > 0) {
  var keys = Object.keys(data[0]).sort((e1, e2) => {
    return (e1 === 'congressional-district' ?
      -3
    : e1 === 'state' && e2 !== 'congressional-district' ?
      -2
    : e1 === 'NAME' && e2 !== 'congressional-district' && e2 !== 'state' ?
      -1
    :
      0
    );
  });
  var headers = keys.map((e) => {
    return e + (variables.variables[e] && variables.variables[e].concept ? ' - ' + variables.variables[e].concept.replace(/\,/g,'.') : '');
  });
  for(var i = 0;i < headers.length;i++) {
    csv += headers[i] + (i === headers.length - 1 ? '\n' : ',');
  }
  for(var i = 0;i < data.length;i++) {
    for(var j = 0;j < keys.length;j++) {
      var tmp = data[i][keys[j]];
      if(typeof tmp === 'string') {
        tmp = tmp.replace(/\,/g,'.');
      }
      csv += tmp + (j === keys.length - 1 ? '\n' : ',');
    }
  }
  fs.writeFileSync('data.csv', csv);
}